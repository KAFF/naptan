package naptan

import "encoding/xml"

type StopArea struct {
	XMLName xml.Name `xml:"StopArea"`

	CreationDateTime      string            `xml:"CreationDateTime,attr"`
	ModificationDateTime  string            `xml:"ModificationDateTime,attr"`
	Modification          string            `xml:"Modification,attr"`
	RevisionNumber        string            `xml:"RevisionNumber,attr"`
	Status                string            `xml:"Status,attr"`
	StopAreaCode          string            `xml:"StopAreaCode"`
	Name                  string            `xml:"Name"`
	AdministrativeAreaRef string            `xml:"AdministrativeAreaRef"`
	StopAreaType          string            `xml:"StopAreaType"`
	Location              Location          `xml:"Location"`
	ParentStopAreaRef     ParentStopAreaRef `xml:"ParentStopAreaRef"`
}

type ParentStopAreaRef struct {
	XMLName xml.Name `xml:"ParentStopAreaRef"`

	Text                 string `xml:",chardata"`
	ModificationDateTime string `xml:"ModificationDateTime,attr"`
	RevisionNumber       string `xml:"RevisionNumber,attr"`
}
