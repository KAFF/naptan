package naptan

import "encoding/xml"

type Network struct {
	XMLName xml.Name `xml:"Network"`

	CreationDateTime     string `xml:"CreationDateTime,attr"`
	ModificationDateTime string `xml:"ModificationDateTime,attr"`
	Modification         string `xml:"Modification,attr"`
	RevisionNumber       string `xml:"RevisionNumber,attr"`
	Status               string `xml:"Status,attr"`
}
