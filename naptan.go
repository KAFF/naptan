package naptan

import "encoding/xml"

const ns = "http://www.naptan.org.uk/"

type NaPTAN struct {
	XMLName xml.Name `xml:"NaPTAN"`

	Xmlns                string `xml:"xmlns,attr"`
	CreationDateTime     string `xml:"CreationDateTime,attr"`
	ModificationDateTime string `xml:"ModificationDateTime,attr"`
	Modification         string `xml:"Modification,attr"`
	RevisionNumber       string `xml:"RevisionNumber,attr"`
	FileName             string `xml:"FileName,attr"`
	SchemaVersion        string `xml:"SchemaVersion,attr"`

	StopPoints []StopPoint `xml:"StopPoints>StopPoint"`
	StopAreas  []StopArea  `xml:"StopAreas>StopArea"`
	Networks   []Network   `xml:"Networks>Network"`
}
