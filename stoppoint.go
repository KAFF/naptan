package naptan

import "encoding/xml"

type StopPoint struct {
	XMLName xml.Name `xml:"StopPoint"`

	CreationDateTime      string             `xml:"CreationDateTime,attr"`
	ModificationDateTime  string             `xml:"ModificationDateTime,attr"`
	Modification          string             `xml:"Modification,attr"`
	RevisionNumber        string             `xml:"RevisionNumber,attr"`
	Status                string             `xml:"Status,attr"`
	AtcoCode              string             `xml:"AtcoCode"`
	NaptanCode            string             `xml:"NaptanCode"`
	Descriptor            Descriptor         `xml:"Descriptor"`
	Place                 Place              `xml:"Place"`
	StopClassification    StopClassification `xml:"StopClassification"`
	AdministrativeAreaRef string             `xml:"AdministrativeAreaRef"`
	PlusbusZones          []PlusbusZoneRef   `xml:"PlusbusZones>PlusbusZoneRef"`
}

type Descriptor struct {
	XMLName xml.Name `xml:"Descriptor"`

	CommonName string `xml:"CommonName"`
	Landmark   string `xml:"Landmark"`
	Street     string `xml:"Street"`
	Indicator  string `xml:"Indicator"`
	Crossing   string `xml:"Crossing"`
}

type Place struct {
	XMLName xml.Name `xml:"Place"`

	NptgLocalityRef    string   `xml:"NptgLocalityRef"`
	LocalityCentre     string   `xml:"LocalityCentre"`
	Location           Location `xml:"Location"`
	Town               string   `xml:"Town"`
	MainNptgLocalities []string `xml:"MainNptgLocalities>NptgLocalityRef"`
	Suburb             string   `xml:"Suburb"`
}

type Location struct {
	XMLName xml.Name `xml:"Location"`

	Translation Translation `xml:"Translation"`
}

type Translation struct {
	XMLName xml.Name `xml:"Translation"`

	GridType  string  `xml:"GridType"`
	Easting   string  `xml:"Easting"`
	Northing  string  `xml:"Northing"`
	Longitude float64 `xml:"Longitude,omitempty"`
	Latitude  float64 `xml:"Latitude,omitempty"`
}

type StopClassification struct {
	XMLName xml.Name `xml:"StopClassification"`

	StopType  string    `xml:"StopType"`
	OnStreet  OnStreet  `xml:"OnStreet"`
	OffStreet OffStreet `xml:"OffStreet"`
}

type OnStreet struct {
	XMLName xml.Name `xml:"OnStreet"`

	Bus Bus `xml:"Bus"`
}

type Bus struct {
	XMLName xml.Name `xml:"Bus"`

	BusStopType       string            `xml:"BusStopType"`
	TimingStatus      string            `xml:"TimingStatus"`
	MarkedPoint       MarkedPoint       `xml:"MarkedPoint"`
	AnnotatedCoachRef AnnotatedCoachRef `xml:"AnnotatedCoachRef"`
	UnmarkedPoint     UnmarkedPoint     `xml:"UnmarkedPoint"`
}

type MarkedPoint struct {
	XMLName xml.Name `xml:"MarkedPoint"`

	Bearing Bearing `xml:"Bearing"`
}

type AnnotatedCoachRef struct {
	XMLName xml.Name `xml:"AnnotatedCoachRef"`

	CreationDateTime     string      `xml:"CreationDateTime,attr"`
	ModificationDateTime string      `xml:"ModificationDateTime,attr"`
	Modification         string      `xml:"Modification,attr"`
	RevisionNumber       string      `xml:"RevisionNumber,attr"`
	CoachRef             string      `xml:"CoachRef"`
	Name                 string      `xml:"Name"`
	LongName             string      `xml:"LongName"`
	Location             Translation `xml:"Location"`
}

type UnmarkedPoint struct {
	XMLName xml.Name `xml:"UnmarkedPoint"`

	Bearing Bearing `xml:"Bearing"`
}

type Bearing struct {
	XMLName xml.Name `xml:"Bearing"`

	CompassPoint string `xml:"CompassPoint"`
	Degrees      string `xml:"Degrees"`
}

type OffStreet struct {
	XMLName xml.Name `xml:"OffStreet"`

	BusAndCoach struct {
		Bay Bay `xml:"Bay"`
	} `xml:"BusAndCoach"`
	Rail Rail `xml:"Rail"`
}

type Bay struct {
	XMLName xml.Name `xml:"Bay"`

	TimingStatus string `xml:"TimingStatus"`
}

type Rail struct {
	XMLName xml.Name `xml:"Rail"`

	Entrance string `xml:"Entrance"`
}

type PlusbusZoneRef struct {
	XMLName xml.Name `xml:"PlusbusZoneRef"`

	CreationDateTime     string `xml:"CreationDateTime,attr"`
	ModificationDateTime string `xml:"ModificationDateTime,attr"`
	Modification         string `xml:"Modification,attr"`
	RevisionNumber       string `xml:"RevisionNumber,attr"`
	Status               string `xml:"Status,attr"`
}
